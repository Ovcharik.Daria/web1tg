/ * jslint browser * /
var mytov = [500, 440, 250];
var myradios = [10, 30, 40];
var mycheckbox = [50, 70];
var PRICE = 0;
var ch;
var radio;

function calc() {
    var select = document.getElementsByName("myselect");
    switch (select[0].value) {
        case "0":
            PRICE = 0;
            break;
        case "1":
            PRICE = mytov[0];
            break;
        case "2":
            PRICE = mytov[1];
            radio = document.getElementsByName("myradio");
            if (radio[0].checked) {
                PRICE += myradios[0];
            }
            if (radio[1].checked) {
                PRICE += myradios[1];
            }
            if (radio[2].checked) {
                PRICE += myradios[2];
            }
            break;
        case "3":
            PRICE = mytov[2];
            ch = document.getElementsByName("mycheckbox");
            if (ch[0].checked) {
                PRICE += mycheckbox[0];
            }
            if (ch[1].checked) {
                PRICE += mycheckbox[1];
            }
            break;
    }
    var kol = document.getElementsByName("kolvo")[0];
    var k = kol.value;
    PRICE *= k;
    var Pr = document.getElementById("price");
    Pr.innerHTML = PRICE + " рублей";
}

window.addEventListener("DOMContentLoaded", function(event) {
    var s = document.getElementsByName("myselect");
    s[0].addEventListener("change", function(event) {
        var select = event.target;
        var rad = document.getElementById("myradios");
        var ch = document.getElementById("mycheckbox");
        console.log(select.value);
        if (select.value === "2") {
            rad.style.display = "block";
        } else {
            rad.style.display = "none";
        }
        if (select.value === "3") {
            ch.style.display = "block";
        } else {
            ch.style.display = "none";
        }
        calc();
    });
    var r = document.getElementsByName("myradio");
    r.forEach(function(radio) {
        radio.addEventListener("change", function(event) {
            var r2 = event.target;
            console.log(r2.value);
            calc();
        });
    });
    var c = document.getElementsByName("mycheckbox");
    c.forEach(function(checkbox) {
        checkbox.addEventListener("change", function(event) {
            var c2 = event.target;
            console.log(c2.value);
            calc();
        });
    });
    var k = document.getElementsByName("kolvo");
    k.forEach(function(kolvo) {
        kolvo.addEventListener("change", function(event) {
            var k2 = event.target;
            console.log(k2.value);
            calc();
        });
    });
});